var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');


var mongoDB = 'mongodb://localhost:27017/scafolder';

mongoose.connect(mongoDB);

mongoose.Promise = global.Promise;

//Load models form app/model to map with MONGODB
var models = {};
fs.readdirSync('app/model/').forEach(function(filename){
    var model = {};
    //TODO: VOy por aqui
    model.path = path.join(APP_PATH, '/app/model/', filename );
    model = mongoose.model(model, model);
    models[model.name] = model;
    console.log("Leidas todos los modelos " + model);
});