//Importamos las librerias necesarias
var express = require('express');
var headersCheck = require('./app/middlewares/headers-check');
var RegistryController = require('./app/controllers/registry.controller');
global.APP_PATH = __dirname;


var registryController = new RegistryController();

var app = express();

//Seteamos el Middleware para comprobar si la cabecera tiene las correspondientes.
app.use(express.json());
app.use(headersCheck);


app.post('/registry', registryController.registry);

app.listen(3000, function(){
    console.log('Example app listen at port 3000!');
});